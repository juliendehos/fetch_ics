#!/bin/sh

OUTFILE=`sed -n 1p config.txt`
PATTERN=`sed -n 2p config.txt`
URLS=`tail -n +3 config.txt`

g++ -Wall -Wextra -std=c++11 -o fetch_ics_cpp1.out fetch_ics_cpp1.cpp

echo "BEGIN:VCALENDAR" > "${OUTFILE}"
for url in ${URLS} ; do
    curl "${url}" \
        | ./fetch_ics_cpp1.out "${PATTERN}" \
        | sed 's/\r$//' \
        >> "${OUTFILE}"
done
echo "END:VCALENDAR" >> "${OUTFILE}"

