#/bin/sh

OUTFILE=`sed -n 1p config.txt`
PATTERN=`sed -n 2p config.txt`
URLS=`tail -n +3 config.txt`

echo "BEGIN:VCALENDAR" > "${OUTFILE}"
for url in ${URLS} ; do
    echo ${url}
    curl -s "${url}" \
        | perl -0777 -pe 's/.*?(BEGIN:VEVENT.*END:VEVENT).*/\1/s' \
        | awk -v RS="END:VEVENT" -v pat="${PATTERN}" 'tolower($0) ~ pat {print; print "END:VEVENT"}' \
        >> "${OUTFILE}"
done
echo "END:VCALENDAR" >> "${OUTFILE}"

