// g++ -Wall -Wextra -std=c++11 -o fetch_ics_cpp1.out fetch_ics_cpp1.cpp
#include <iostream>
#include <regex>
#include <string>
#include <vector>
using namespace std;

int main(int argc, char ** argv) {
    if (argc != 2) {
        cerr << "usage: " << argv[0] << " <pattern>" << endl;
        exit(-1);
    }

    string line;

    // ignore header
    std::regex beginRegex("^BEGIN:VEVENT", regex_constants::icase);
    while (getline(cin, line) and not regex_search(line, beginRegex)) {}

    // read and filter events
    string event = line;
    bool patternFound = false;
    // read a line
    while (getline(cin, line)) {
        // remember the line
        event += line + "\n";
        // check if the line matches pattern
        std::regex patternRegex(argv[1], regex_constants::icase);
        if (std::regex_search(line, patternRegex)) 
            patternFound = true;
        // if end of event, display event if pattern found and begin new event
        std::regex endRegex("^END:VEVENT", regex_constants::icase);
        if (std::regex_search(line, endRegex)) {
            if (patternFound) 
                cout << event << "\n";
            event = "";
            patternFound = false;
        }
    }

    return 0;
}

