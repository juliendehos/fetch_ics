import Data.Char
import Data.List
import Text.Regex.PCRE
import Network.Curl.Download

filterEvents _ (Left _) = []
filterEvents pattern (Right s) = intercalate "\n" events
    where matches = s =~ "BEGIN:VEVENT(.|\n)*?END:VEVENT" :: [[String]]
          predicate = (isInfixOf pattern) . (map toLower)
          events = filter predicate (map head matches)

format events = "BEGIN:VCALENDAR\n" ++ (intercalate "\n" events ) ++ "END:VCALENDAR"

main = do
    configfile <- readFile "config.txt" 
    let (outfile:pattern:urls) = lines configfile
    mapM openURIString urls 
        >>= (writeFile outfile) . format . (map (filterEvents pattern))

