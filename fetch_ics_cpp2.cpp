// g++ -Wall -Wextra -std=c++11 -o fetch_ics_cpp2.out fetch_ics_cpp2.cpp -lcurlcpp -lcurl
#include <curl_easy.h>
#include <curl_ios.h>
#include <curl_exception.h>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
using namespace std;

void filterStream(istream & is, ostream & os, const string & pattern) {
    string line;
    // ignore header
    regex beginRegex("^BEGIN:VEVENT", regex_constants::icase);
    while (getline(is, line) and not regex_search(line, beginRegex)) {}
    // read and filter events
    string event = line;
    bool patternFound = false;
    // read lines
    while (getline(is, line)) {
        event += line + "\n";
        // check if the line matches pattern
        regex patternRegex(pattern, regex_constants::icase);
        if (regex_search(line, patternRegex)) 
            patternFound = true;
        // if end of event, display event if pattern found and begin new event
        regex endRegex("^END:VEVENT", regex_constants::icase);
        if (regex_search(line, endRegex)) {
            if (patternFound) 
                os << event << "\n";
            event = "";
            patternFound = false;
        }
    }
}

int main() {
    // read config file
    ifstream configfile("config.txt");
    string outfile;
    getline(configfile, outfile);
    string pattern;
    getline(configfile, pattern);
    string url;

    // download and filter ICS
    ofstream ofs(outfile);
    ofs << "BEGIN:VCALENDAR" << endl;
    while (getline(configfile, url)) {
        cout << url << endl;
        stringstream ss;
        curl::curl_ios<stringstream> writer(ss);
        curl::curl_easy easy(writer);
        easy.add<CURLOPT_URL>(url.c_str());
        easy.perform();
        filterStream(ss, ofs, pattern);
    }
    ofs << "END:VCALENDAR" << endl;
    return 0;
}

