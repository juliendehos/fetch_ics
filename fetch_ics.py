#!/usr/bin/env python3
import re
import urllib.request

configfile = open('config.txt', 'r')
outname = configfile.readline().strip()
pattern = configfile.readline().strip().lower()

outfile = open(outname, 'w')
outfile.write('BEGIN:VCALENDAR\n')
for url in configfile:
    print(url)
    response = urllib.request.urlopen(url)
    ics = str(response.read(), 'utf-8')
    events = re.findall('BEGIN:VEVENT.*?END:VEVENT', ics, re.DOTALL)
    for event in events:
        if re.search(pattern, event.lower()):
            outfile.write(event)
            outfile.write('\n')
outfile.write('END:VCALENDAR\n')

