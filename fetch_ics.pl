#!/usr/bin/env perl
use LWP::Simple;

open my $configfile, '<', "config.txt"; 
chomp(my $OUTFILE = <$configfile>); 
chomp(my $PATTERN = <$configfile>); 

open my $out, '>:encoding(UTF-8)', $OUTFILE;
print $out "BEGIN:VCALENDAR\n";
for my $url (<$configfile>) {
    print "$url\n";
    my $ics = get $url;
    my @events = $ics =~ m/(BEGIN:VEVENT[\s\S]*?END:VEVENT)/g;
    for $e (@events) {
        if ($e =~ /$PATTERN/i) {
            print $out "${e}\n"; 
        }
    }
}
print $out "END:VCALENDAR\n";

