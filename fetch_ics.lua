#!/usr/bin/env lua
curl = require "curl"

configfile = io.open("config.txt", "r")
outname = configfile:read()
pattern = configfile:read()

outfile = io.open(outname, "w")
outfile:write("BEGIN:VCALENDAR\n")
for url in configfile:lines() do
    local data = ""
    local c = curl.easy_init()
    c:setopt(curl.OPT_URL, url)
    c:setopt(curl.OPT_WRITEFUNCTION, 
        function (str,len) data = data .. str return len,nil end)
    c:perform()
    for event in data:gmatch("BEGIN:VEVENT.-END:VEVENT") do
        if event:lower():find(pattern) then
            outfile:write(event, "\n")
        end
    end
end
outfile:write("END:VCALENDAR\n")

