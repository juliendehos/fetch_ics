import Data.Char
import Data.List
import Data.List.Split
import Network.Curl.Download

filterEvents _ (Left _) = []
filterEvents pattern (Right y) = concat $ filter predicate splits
    where events = unlines $ takeWhile (/= "END:VCALENDAR") $ dropWhile (/= "BEGIN:VEVENT") $ lines $ filter (/= '\r') y
          eventSplitter = split . keepDelimsR . onSublist 
          splits = eventSplitter "END:VEVENT\n" events
          predicate s = isInfixOf pattern (map toLower s)

format x = "BEGIN:VCALENDAR\n" ++ (concat x) ++ "END:VCALENDAR"

main = do
    configfile <- readFile "config.txt" 
    let (outfile:pattern:urls) = lines configfile
    mapM openURIString urls 
        >>= (writeFile outfile) . format . (map (filterEvents pattern))

