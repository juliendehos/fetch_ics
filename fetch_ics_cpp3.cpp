// g++ -Wall -Wextra -std=c++11 -o fetch_ics_cpp3.out fetch_ics_cpp3.cpp -lboost_system -lboost_thread -lcppnetlib-uri -lcppnetlib-client-connections -lssl -lcrypto -lpthread 
#define BOOST_NETWORK_ENABLE_HTTPS
#include <boost/network/protocol/http/client.hpp>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
namespace http = boost::network::http;
using namespace std;

void filterStream(istream & is, ostream & os, const string & pattern) {
    string line;
    // ignore header
    regex beginRegex("^BEGIN:VEVENT", regex_constants::icase);
    while (getline(is, line) and not regex_search(line, beginRegex)) {}
    // read and filter events
    string event = line;
    bool patternFound = false;
    // read lines
    while (getline(is, line)) {
        event += line + "\n";
        // check if the line matches pattern
        regex patternRegex(pattern, regex_constants::icase);
        if (regex_search(line, patternRegex)) 
            patternFound = true;
        // if end of event, display event if pattern found and begin new event
        regex endRegex("^END:VEVENT", regex_constants::icase);
        if (regex_search(line, endRegex)) {
            if (patternFound) 
                os << event << "\n";
            event = "";
            patternFound = false;
        }
    }
}

int main() {
    // read config file
    ifstream configfile("config.txt");
    string outfile;
    getline(configfile, outfile);
    string pattern;
    getline(configfile, pattern);
    string url;

    // download and filter ICS
    ofstream ofs(outfile);
    ofs << "BEGIN:VCALENDAR" << endl;
    while (getline(configfile, url)) {
        cout << url << endl;
        boost::shared_ptr<boost::asio::io_service> pService 
            = boost::make_shared<boost::asio::io_service>();
        http::client client(http::client::options().io_service(pService));
        http::client::request request(url);
        http::client::response response = client.get(request);
        istringstream iss(body(response));
        filterStream(iss, ofs, pattern);
        pService->stop();
    }
    ofs << "END:VCALENDAR" << endl;
    return 0;
}

