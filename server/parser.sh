#!/bin/sh

URLS=`cat urls.txt`

# HTTP response
echo "HTTP/1.0 200 OK"
echo "Content-Type: text/html; charset=UTF-8"
echo "Content-Encoding: UTF-8"
echo ""

# content
PATTERN=`echo "${RQ_URL}" | sed 's/\///'`

echo "BEGIN:VCALENDAR" 
for url in ${URLS} ; do
    curl "${url}" \
        | perl -0777 -pe 's/.*?(BEGIN:VEVENT.*END:VEVENT).*/\1/s' \
        | awk -v RS="END:VEVENT" -v pat="${PATTERN}" 'tolower($0) ~ pat {print; print "END:VEVENT"}' \
        | sed 's/\r$//' \
        | sed '/^\ *$/d'  
done
echo "END:VCALENDAR" 

