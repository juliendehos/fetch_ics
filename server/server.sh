#!/bin/sh

PORT="8080"

while [ $? -eq 0 ] ; do 
    #nc -vlp ${PORT} -e "./parser.sh" 
    socat TCP4-LISTEN:${PORT},fork EXEC:"./parser.sh"
done

